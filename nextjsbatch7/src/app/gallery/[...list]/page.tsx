"use client"
import { Container } from '@components';
import Satellite from '@services/satellite';
import Image from 'next/image';
import React, { useEffect, useState } from 'react'

export default function GaleryDetails({
    params,
}: {
    params: { list: string };
}) {

  const config = {
    headers: {
      Authorization: `Bearer ${process.env.TOKEN}`
    }
  }
  
  const getDetailMovie = async () => {
    let res;
    await Satellite.get(`https://api.themoviedb.org/3/movie/${params.list}`, config)
      .then((response) => {
        res = response.data
        console.log(res)
      })
      .catch((error) => {
        console.log(error);
      })
    return res
  }
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    const fetchData = async() => {
      const data = await getDetailMovie();
      setDataMovie([data] as any);
    }
    fetchData()
  }, []);
  return (
    <div>
      <Container>
          <h1 className='text-4xl'>
              {dataMovie && dataMovie.map((data : any) => (
                <div key={data.id} className='flex flex-col'>
                  <h1 className='text-2xl lg:text-4xl'>Details:  {data.title}</h1>
                  <div className='grid grid-cols-1 md:grid-cols-2 gap-5 my-5'>
                    <div className='flex items-center justify-center'>
                      <Image
                        src={`https://image.tmdb.org/t/p/w500/${data.poster_path}`}
                        width={600}
                        height={600}
                        alt='Poster Movie'
                      />
                    </div>
                    <div>
                      <ul style={{ fontSize: 30}}>
                        <li><b className='mr-2'>Adult</b> : {data.adult ? "Yes" : "No"}</li>
                        <li><b className='mr-2'>Budget</b> : {data.budget}</li>
                        <li><b className='mr-2'>Genres</b>  : {data.genres.map((genre : any) => (
                          ` ${genre.name}`
                        ))}</li>
                        <li><b className='mr-2'>Overview</b> : {data.overview}</li>
                      </ul>
                    </div>
                  </div>
                </div>
              ))}
          </h1>
      </Container>
    </div>
  )
}
