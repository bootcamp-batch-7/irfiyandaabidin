"use client";
import { Container } from "@components";
import Satellite from "@services/satellite";
import { useEffect, useState } from "react";
import env from "react-dotenv";
import ListMoviePopuler from "./listMoviePopuler";
import ListMovieTopRated from "./listMovieTopRated";
import ListMovieUpcoming from "./listMovieUpcoming";
import ListMovieNowPlaying from "./listMovieNowPlaying";

// export const metadata = {
//     title: "Gallery"
// }

export default function Gallery() {
  const [sortBy, setSortBy] = useState("Popular");

  // console.log(dataPopular)
  return (
    <Container>
      <div className="min-h-screen flex flex-1 flex-col">
        <h1 className="text-4xl font-bold">Gallery</h1>
        <div className="mt-5">
          <label htmlFor="sortBy" className="mr-3">
            Sort By:
          </label>
          <select
            id="sortBy"
            name=""
            value={sortBy}
            onChange={(e) => setSortBy(e.target.value)}
            className="custom-shadow text-black"
          >
            <option value="Popular">Popular</option>
            <option value="Top Rated">Top Rated</option>
            <option value="Upcoming">Upcoming</option>
            <option value="Now Playing">Now Playing</option>
          </select>
        </div>
        {sortBy === "Popular" ? <ListMoviePopuler /> : <div></div>}
        {sortBy === "Top Rated" ? <ListMovieTopRated /> : <div></div>}
        {sortBy === "Upcoming" ? <ListMovieUpcoming /> : <div></div>}
        {sortBy === "Now Playing" ? <ListMovieNowPlaying /> : <div></div>}
      </div>
    </Container>
  );
}
