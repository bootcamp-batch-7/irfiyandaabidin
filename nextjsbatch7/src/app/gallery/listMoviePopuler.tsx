import Satellite from "@services/satellite";
import Image from "next/image";
import Link from "next/link";
import { useState, useEffect } from "react";

const config = {
  headers: {
    Authorization: `Bearer ${process.env.TOKEN}`,
  },
};

const getMoviePopular = async () => {
  let res;
  await Satellite.get(`https://api.themoviedb.org/3/movie/popular`, config)
    .then((response) => {
      res = response.data.results;
    })
    .catch((error) => {
      console.log(error);
    });
  return res;
};

function ListMoviePopuler() {
  const [dataPopular, setDataPopular] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const data = await getMoviePopular();
      setDataPopular(data as any);
      setIsLoading(false)
    };
    fetchData();
  }, []);

  if(isLoading) {
    return <p>Loading...</p>
  }
  return (
    <div>
      <h1 className="text-2xl m-5">List Popular Movies : </h1>
      <div className="flex flex-wrap">
        {dataPopular.map((data : any) => (
          <Link href={`/gallery/${data.id}`} key={data.id} className="relative flex justify-center m-2 h-52 w-40 text-center" style={{ backgroundSize: "cover", backgroundPosition: "center", backgroundImage: `url(https://image.tmdb.org/t/p/w500/${data.poster_path})`}}>
            <p className="block w-full bottom-0 absolute opacity-0 hover:bg-opacity-75 hover:bg-slate-800 hover:opacity-100">{data.title}</p>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default ListMoviePopuler;
