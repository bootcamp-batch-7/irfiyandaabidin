// "use client"
import React from 'react'
import Satellite from '@services/satellite'

const getUserData = async(param: string) => {
  let res;
  await Satellite.get(`https://api.github.com/search/users?q=${param}`)
  .then((response) => {
    console.log(response.data.items)
    res=response.data.items
  }).catch((error) => {
    console.log("ERROR GET DATA ", error)
  })
  return res;
}

export default async function UserDetail({params} : {params: {slug : string}}) {
  const data = (await getUserData(params.slug)) as any;
  return (
    <div className='flex flex-1 justify-center items-center min-h-screen flex-col'>
      <h1 className='text-6xl font-bold'>Detail User : { params.slug }</h1>
      <h1 className='text-4xl font-bold'>ID: {data[0].id || "-"}</h1>
      <h1 className='text-4xl font-bold'>Email: {data[0].email || "-"}</h1>
      <a href={data[0].html_url} className='text-4xl font-bold'>Git URL: {data[0].html_url || "-"}</a>
      <h1 className='text-4xl font-bold'>Location: {data[0].location || "-"}</h1>
    </div>
  )
}
