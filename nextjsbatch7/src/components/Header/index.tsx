'use client'
import Link from "next/link";
import Image from "next/image";
import images from "@assets/images";
import { useEffect, useState } from "react";
import { usePathname } from "next/navigation"; 
import { GetStorage, SetStorage } from "@services/storage";

export default function Header() {

    const pathName = usePathname();

    const localTheme = GetStorage("theme")

    const [isDark, setIsDark] = useState(localTheme === "dark")

    const menuHeader = [
        {name: "Gallery", href: "/gallery"},
        {name: "About Us", href: "/about"},
        {name: "Users", href: "/users"},
    ];

    useEffect(() => {
        if(localTheme === "light" || localTheme === "dark"){
            setIsDark(localTheme === "dark");
        } else {
            const getCurrentTheme = () =>
            window.matchMedia("(prefers-color-scheme: dark)").matches;
            setIsDark(getCurrentTheme());
        }
    }, [])

    useEffect(() => {
        if(isDark){
            document.body.classList.add("dark")
        } else {
            document.body.classList.remove("dark")
        }
        SetStorage("theme", isDark ? "dark" : "light")
    }, [isDark])

    return (
    <header className="px-5 flex justify-between flex-row custom-shadow">
        <Link href="/" className="flex flex-row py-5">
            <Image
                src={images.LOGO_BERIJALAN}
                width={30}
                height={30}
                alt="logo berijalan"
            />
            <h1 className="text-4xl bold font-extrabold">
                berijalan
            </h1>
        </Link>

        <ul className="flex flex-row">
        <div className="p-5 my-2" style={{ cursor: "pointer" }} onClick={() => setIsDark(!isDark)}>
            {isDark ? (
                <Image
                src={images.LIGHT}
                width={24}
                height={24}
                alt="logo light"
            />
                ) : (
                <Image
                    src={images.DARK}
                    width={24}
                    height={24}
                    alt="logo moon"
                />
            )}
        </div>
            {menuHeader.map((item, index) => (
                <Link key={index} href={item.href}>
                    <li className={`${pathName === item.href ? "header-selection" : "" } my-2 rounded-md hover:header-selection py-5 min-w-[100px] text-center justify-center `}>{item.name}</li>
                </Link>
            ))}
        </ul>
    </header>
  )
}
