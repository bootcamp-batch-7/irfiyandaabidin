const images = {
    LOGO_BERIJALAN: "/images/logo_berijalan.png",
    DARK: "/images/icon_moon.png",
    LIGHT: "/images/icon_sun.png",
}

export default images