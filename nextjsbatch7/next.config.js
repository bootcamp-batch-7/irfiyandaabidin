/** @type {import('next').NextConfig} */
require('dotenv').config();
const nextConfig = {
    images: {
        domains: ["avatars.githubusercontent.com", "image.tmdb.org"]
    },
    env: {
        TOKEN: process.env.TOKEN
    },
}

module.exports = nextConfig
