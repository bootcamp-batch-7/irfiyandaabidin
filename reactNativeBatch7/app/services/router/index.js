import { createStackNavigator } from '@react-navigation/stack'
import React from 'react';
import Main from '../../pages/Main'
import Login from '../../pages/Login';
import Register from '../../pages/Register';

const Stack = createStackNavigator()

const Routes = () => {
  return (
    <Stack.Navigator 
      screenOptions={{headerShown:false}}
      initialRouteName='Login'
    >
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="Main" component={Main}/>
        <Stack.Screen name="Register" component={Register}/>
    </Stack.Navigator>
  )
}

export default Routes;