import axios from "axios";

const Satellite = axios.create({
    baseURL: "https://be-rmy.dev.berijalan.co.id/rest/v1/",
    timeout: 10000, // millisecond
    headers: {
        "Content-Type": "application/json",
        "Access-Controll-Allow-Origin" : "*",
    }
})

Satellite.interceptors.response.use(
    (response) => {
        return response
},
    (error) => {
    // console.log("ERROR SATELLITE ", JSON.stringify(error, null, 2));
})

export default Satellite;