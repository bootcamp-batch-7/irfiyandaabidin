import { View, Text } from 'react-native'
import React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'

export default function IndexLayout() {
  return (
    <View style={{marginTop: 40, marginHorizontal: 16}}>
      <Text>App</Text>
    </View>
  )
}