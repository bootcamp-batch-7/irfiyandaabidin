import React from 'react'
import {  TextInput, View } from 'react-native'
import Text from '../../components/Text'

function AddTask() {
  return (
    <View style={{marginTop: 44, marginHorizontal: 16}}>
      <Text fontSize={24} bold>
        Time Sheet Form
      </Text>
      <View style={{ marginTop: 16}}>
        <Text fontSize={16} bold>Title</Text>
        <TextInput placeholder={"Enter Task Title"} style={{ padding: 12, borderColor: "#A7A7A7", marginTop: 8, marginBottom: 27,borderWidth: 0.5, borderRadius: 8}}/>
      </View>
      <View style={{backgroundColor: "blue"}}>
        
      </View>
    </View>
  )
}

export default AddTask