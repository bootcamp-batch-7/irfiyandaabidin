import React, { useEffect, useState } from "react";
import { ScrollView, TextInput, View } from "react-native";
import Text from "../../components/Text";
import Images from "../../assets/img/index";
import { ImageBackground } from "react-native";
import { HEIGHT, Fonts } from "../../assets/styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { EyeIcon, EyeSlashIcon, Arr, ArrowLeftIcon } from "../../assets/svg";
import Satellite from "../../services/satellite";

function Register({ navigation, router }) {
  const [isEnable, setIsEnable] = useState(false);
  const [showPassword, setShowPassword] = useState(true);
  const [showConfirmPassword, setShowConfirmPassword] = useState(true);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [nik, setNik] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPass, setConfirmPass] = useState("");

  const [errorName, setErrorName] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorPhone, setErrorPhone] = useState("");
  const [errorNik, setErrorNik] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const [errorConfirmPass, setErrorConfirmPass] = useState("");

  const handlerShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const onSubmit = async () => {
    const body = {
      doSendRegister: {
        name,
        email,
        phoneNumber: phone,
        nik,
        password,
      },
    };
    await Satellite.post("auth/register", body)
      .then((res) => {
        if (res == undefined) {
          console.log("undefined");
        }
        // console.log(res)
        res.status == 200 ? navigation.navigate("Login") : error;
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (
      email !== "" &&
      phone !== "" &&
      nik !== "" &&
      password !== "" &&
      confirmPass !== "" &&
      name !== "" &&
      errorConfirmPass === "" &&
      errorPassword === "" &&
      errorName === "" &&
      errorEmail === "" &&
      errorNik === "" &&
      errorPhone === ""
    ) {
      setIsEnable(true);
    } else {
      setIsEnable(false);
    }
  });
  return (
    <View>
      <ImageBackground style={{ height: HEIGHT }} source={Images.BG_SCREEN}>
        <ScrollView>
          <View style={{ paddingHorizontal: 31 }}>
            <TouchableOpacity onPress={() => navigation.navigate("Login")}>
              <ArrowLeftIcon
                style={{ marginTop: 54 }}
                widhth={20}
                height={14}
              />
            </TouchableOpacity>
            <View style={{ marginTop: 54, paddingBottom: 20 }}>
              <Text
                fontSize={16}
                bold
                color="#D3D3D3"
                style={{ paddingBottom: 8 }}
              >
                Name
              </Text>
              <TextInput
                autoCapitalize="none"
                style={{
                  fontFamily: Fonts.Nunito.Bold,
                  backgroundColor: "#273C75",
                  borderRadius: 8,
                  padding: 12,
                  borderColor: "#132040",
                  color: "#D3D3D3",
                }}
                onChangeText={(value) => {
                  if (value) {
                    setName(value);
                    return setErrorName("");
                  }
                  return setErrorName("Name must be Filled In");
                }}
                placeholder="Enter Your Name"
                placeholderTextColor={"#D3D3D3"}
              ></TextInput>
              <Text
                color="#EA8685"
                fontSize={10}
                style={{ textAlign: "right", marginRight: 8 }}
              >
                {errorName}
              </Text>
            </View>
            <View style={{ paddingBottom: 20 }}>
              <Text
                fontSize={16}
                bold
                color="#D3D3D3"
                style={{ paddingBottom: 8 }}
              >
                Email
              </Text>
              <TextInput
                autoCapitalize="none"
                style={{
                  fontFamily: Fonts.Nunito.Bold,
                  backgroundColor: "#273C75",
                  borderRadius: 8,
                  padding: 12,
                  borderColor: "#132040",
                  color: "#D3D3D3",
                }}
                onChangeText={(value) => {
                  setEmail(value);
                  const regexEmail =
                    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                  if (value === "") {
                    setErrorEmail("email must be filled in");
                    return;
                  }
                  if (!regexEmail.test(value)) {
                    setErrorEmail("Invalid Mail Address");
                    return;
                  } else {
                    setErrorEmail("");
                  }
                }}
                placeholder="Enter Your Email"
                placeholderTextColor={"#D3D3D3"}
              ></TextInput>
              <Text
                color="#EA8685"
                fontSize={10}
                style={{ textAlign: "right", marginRight: 8 }}
              >
                {errorEmail}
              </Text>
            </View>
            <View style={{ paddingBottom: 20 }}>
              <Text
                fontSize={16}
                bold
                color="#D3D3D3"
                style={{ paddingBottom: 8 }}
              >
                Phone
              </Text>
              <TextInput
                keyboardType="numeric"
                maxLength={12}
                style={{
                  fontFamily: Fonts.Nunito.Bold,
                  backgroundColor: "#273C75",
                  borderRadius: 8,
                  padding: 12,
                  borderColor: "#132040",
                  color: "#D3D3D3",
                }}
                placeholder="Enter Your Phone"
                placeholderTextColor={"#D3D3D3"}
                onChangeText={(value) => {
                  if (value.length > 10 && value.length <= 12) {
                    setPhone(value);
                    return setErrorPhone("");
                  }
                  if (!value) {
                    return setErrorPhone("Phone Must be Filled In");
                  }
                  return setErrorPhone("Phone must be 11 or 12 Digit");
                }}
              ></TextInput>
              <Text
                color="#EA8685"
                fontSize={10}
                style={{ textAlign: "right", marginRight: 8 }}
              >
                {errorPhone}
              </Text>
            </View>
            <View style={{ paddingBottom: 20 }}>
              <Text
                fontSize={16}
                bold
                color="#D3D3D3"
                style={{ paddingBottom: 8 }}
              >
                NIK Number
              </Text>
              <TextInput
                keyboardType="numeric"
                maxLength={16}
                style={{
                  fontFamily: Fonts.Nunito.Bold,
                  backgroundColor: "#273C75",
                  borderRadius: 8,
                  padding: 12,
                  borderColor: "#132040",
                  color: "#D3D3D3",
                }}
                placeholder="Enter Your NIK Number"
                placeholderTextColor={"#D3D3D3"}
                onChangeText={(value) => {
                  if (value.length == 16) {
                    setNik(value);
                    return setErrorNik("");
                  }
                  if (!value) {
                    return setErrorNik("NIK Must be Filled In");
                  }
                  return setErrorNik("NIK Must be 16 Digit");
                }}
              ></TextInput>
              <Text
                color="#EA8685"
                fontSize={10}
                style={{ textAlign: "right", marginRight: 8 }}
              >
                {errorNik}
              </Text>
            </View>
            <View style={{ paddingBottom: 20 }}>
              <Text
                fontSize={16}
                bold
                color="#D3D3D3"
                style={{ paddingBottom: 8 }}
              >
                Password
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  backgroundColor: "#273C75",
                  borderRadius: 8,
                  padding: 12,
                  borderColor: "#132040",
                }}
              >
                <TextInput
                  color
                  secureTextEntry={showPassword}
                  style={{ fontFamily: Fonts.Nunito.Bold, color: "#D3D3D3" }}
                  placeholder="Password"
                  placeholderTextColor={"#D3D3D3"}
                  onChangeText={(value) => {
                    if (!value) {
                      return setErrorPassword("Password Must be Filled In");
                    }
                    if (value.length < 6) {
                      return setErrorPassword("Password Min 6 Character");
                    }
                    setPassword(value);
                    return setErrorPassword("");
                  }}
                ></TextInput>
                <TouchableOpacity onPress={() => handlerShowPassword()}>
                  {showPassword ? (
                    <EyeIcon width={20} height={20} />
                  ) : (
                    <EyeSlashIcon width={20} height={20} />
                  )}
                </TouchableOpacity>
              </View>
              <Text
                color="#EA8685"
                fontSize={10}
                style={{ textAlign: "right", marginRight: 8 }}
              >
                {errorPassword}
              </Text>
            </View>
            <View style={{ paddingBottom: 20 }}>
              <Text
                fontSize={16}
                bold
                color="#D3D3D3"
                style={{ paddingBottom: 8 }}
              >
                Confirm Password
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  backgroundColor: "#273C75",
                  borderRadius: 8,
                  padding: 12,
                  borderColor: "#132040",
                }}
              >
                <TextInput
                  color
                  secureTextEntry={showConfirmPassword}
                  style={{ fontFamily: Fonts.Nunito.Bold, color: "#D3D3D3" }}
                  placeholder="Confirm Password"
                  placeholderTextColor={"#D3D3D3"}
                  onChangeText={(value) => {
                    if (value !== password) {
                      console.log(value, password);
                      return setErrorConfirmPass("Password do not match");
                    }
                    if (!value) {
                      return setErrorConfirmPass(
                        "Confirm Password Must be Filled In"
                      );
                    }
                    setConfirmPass(value);
                    setErrorConfirmPass("");
                  }}
                ></TextInput>
                <TouchableOpacity
                  onPress={() => setShowConfirmPassword(!showConfirmPassword)}
                >
                  {showConfirmPassword ? (
                    <EyeIcon width={20} height={20} />
                  ) : (
                    <EyeSlashIcon width={20} height={20} />
                  )}
                </TouchableOpacity>
              </View>
              <Text
                color="#EA8685"
                fontSize={10}
                style={{ textAlign: "right", marginRight: 8 }}
              >
                {errorConfirmPass}
              </Text>
            </View>
            <TouchableOpacity
              disabled={!isEnable}
              onPress={onSubmit}
              style={{
                opacity: 1,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: isEnable ? "#18DCFF" : "#2281B0",
                borderRadius: 8,
                marginBottom: 90,
                marginTop: 27,
                paddingVertical: 12,
              }}
            >
              <Text bold>Register</Text>
            </TouchableOpacity>
            <View style={{ alignItems: "center" }}>
              <Text color="#D3D3D3" bold style={{ marginTop: 20 }}>
                Dont Have an Account?
                <Text color="#F6E58D" bold>
                  Sign Up
                </Text>
              </Text>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  );
}

export default Register;
