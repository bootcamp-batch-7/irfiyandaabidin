import React from 'react'
import Text from '../../components/Text'
import { ScrollView, View } from 'react-native'
import { FlagIcon, HomeIcon, PerformIcon, TaskIcon, TaskIcon1 } from '../../assets/svg'
import { TouchableOpacity } from 'react-native-gesture-handler'


function Task() {
  const dataTask = [
    {
      title: "Develop New Features Mutual Fund Moxa Konven",
      desc: "Aliquip proident reprehenderit Lorem amet laboris elit. Labore ullamco deserunt est magna sunt qui pariatur amet est commodo. Occaecat commodo laboris sunt anim incididunt labore cupidatat exercitation dolore ad aliquip et incididunt aliquip. Eiusmod dolor nisi mollit est eiusmod tempor eiusmod. Quis ad in ipsum et. Dolore laborum mollit labore dolore irure consequat sunt labore deserunt. Qui occaecat quis aliqua laborum id dolore excepteur laboris nisi officia dolore.",
      date: "April 04, 2023",
      time: "08.00 - 17.00"
    },
    {
      title: "Fixing Issue Mobile",
      desc: "Aliquip proident reprehenderit Lorem amet laboris elit. Labore ullamco deserunt est magna sunt qui pariatur amet est commodo. Occaecat commodo laboris sunt anim incididunt labore cupidatat exercitation dolore ad aliquip et incididunt aliquip. Eiusmod dolor nisi mollit est eiusmod tempor eiusmod. Quis ad in ipsum et. Dolore laborum mollit labore dolore irure consequat sunt labore deserunt. Qui occaecat quis aliqua laborum id dolore excepteur laboris nisi officia dolore.",
      date: "April 03, 2023",
      time: "08.00 - 17.00"
    },
    {
      title: "On Leave",
      desc: "Aliquip proident reprehenderit Lorem amet laboris elit. Labore ullamco deserunt est magna sunt qui pariatur amet est commodo. Occaecat commodo laboris sunt anim incididunt labore cupidatat exercitation dolore ad aliquip et incididunt aliquip. Eiusmod dolor nisi mollit est eiusmod tempor eiusmod. Quis ad in ipsum et. Dolore laborum mollit labore dolore irure consequat sunt labore deserunt. Qui occaecat quis aliqua laborum id dolore excepteur laboris nisi officia dolore.",
      date: "April 02, 2023",
      time: "08.00 - 17.00"
    }
  ]

  return (
    <View style={{marginHorizontal: 16}}>
      <ScrollView>
        <Text fontSize={30} bold style={{marginTop : 48, marginBottom: 18}}>Time Sheet</Text>
        <View>
          {dataTask.map((item) => (
            <View style={{borderRadius: 12, borderWidth: 1, borderColor: "#A7A7A7", padding: 16, marginBottom: 16}}>
              <View style={{ flexDirection: "row", alignItems:"center"}}>
                <TaskIcon1 fill="#f00" height={28} width={28} style={{marginRight: 10}}/>
                <Text bold fontSize={18} style={{marginBottom: 0}}>{item.title}</Text>
              </View>
              <View style={{flexDirection: "row", marginTop: 18}}>
                <FlagIcon width={12} style={{marginRight: 10}}/>
                <Text color='#7A7A7A'>{item.date}</Text>
              </View>
              <Text>
                {item.desc}
              </Text>
              <View style={{ marginRight: 174, marginTop: 9, flexDirection: "row" }}>
                <TouchableOpacity style={{paddingHorizontal: 10, paddingVertical: 2, backgroundColor: "#70A1FF", borderRadius: 8 }}>
                  <Text color='#fff'>
                    Present
                  </Text>
                </TouchableOpacity>
                <View style={{marginLeft: 11, borderRadius: 8, backgroundColor: "#CED1D4", paddingVertical: 2, paddingHorizontal: 10}}>
                  <Text color='#fff'>{item.time}</Text>
                </View>
              </View>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>    
  )
}

export default Task