import { ImageBackground, View, Image, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import Text from "../../components/Text/index";
import images from "../../assets/img/index";
import { WIDTH } from "../../assets/styles";
import { ScrollView } from "react-native";
import Svg, { Path, Circle } from "react-native-svg";
import { LinearGradient } from "expo-linear-gradient";
import { HomeIcon } from "../../assets/svg";
import Navigation from "../../components/Navigation";

const Profile = () => {
  return (
    <View style={{ backgroundColor: "#fff" }}>
      <ScrollView>
      <ImageBackground
        source={images.BG_PROFILE}
        style={{
          width: WIDTH,
          height: 495,
          paddingTop: 80,
          alignItems: "center",
        }}
      >
        {/* Image Profile */}
        <View
          style={{borderWidth: 2, borderColor: "#FBD2A5", borderRadius: 90 }}
        >
          <Image
            source={images.PERSON2}
            style={{
              width: 120,
              height: 120,
              borderRadius: 90,
              alignItems: "center",
            }}
          />
        </View>
        <Text fontSize={20} bold style={{ marginTop: 16 }}>
          Irfiyanda Abidin
        </Text>
        <Text fontSize={14} color={"#909090"}>
          Backend Engineering
        </Text>
        </ImageBackground>

          {/* Colom profile */}
          <View
            style={{
              backgroundColor: "#fff",
              width: 343,
              height: 195,
              borderRadius: 12,
              marginHorizontal: 12,
              marginTop: -200,
              marginBottom: 32,
              shadowColor: "#000",
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
            }}
          >
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                borderBottomColor: "#D3D3D3",
                borderBottomWidth: 2,
                marginHorizontal: 16,
                paddingVertical: 13,
              }}
            >
              <Text style={{ paddingLeft: 14, paddingRight: 19 }}>ID</Text>
              <Text
                style={{ paddingLeft: 14, paddingRight: 19 }}
                color="#A7A7A7"
              >
                210623
              </Text>
            </View>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                borderBottomColor: "#D3D3D3",
                borderBottomWidth: 2,
                marginHorizontal: 16,
                paddingVertical: 13,
              }}
            >
              <Text style={{ paddingLeft: 14, paddingRight: 19 }}>Email</Text>
              <Text
                style={{ paddingLeft: 14, paddingRight: 19 }}
                color="#A7A7A7"
              >
                irfiyanda@gmail.com
              </Text>
            </View>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                borderBottomColor: "#D3D3D3",
                borderBottomWidth: 2,
                marginHorizontal: 16,
                paddingVertical: 13,
              }}
            >
              <Text style={{ paddingLeft: 14, paddingRight: 19 }}>
                Date of Birth
              </Text>
              <Text
                style={{ paddingLeft: 14, paddingRight: 19 }}
                color="#A7A7A7"
              >
                08 April 2003
              </Text>
            </View>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                marginHorizontal: 16,
                marginVertical: 13,
              }}
            >
              <Text style={{ paddingLeft: 14, paddingRight: 19 }}>Gender</Text>
              <Text
                style={{ paddingLeft: 14, paddingRight: 19 }}
                color="#A7A7A7"
              >
                Male
              </Text>
            </View>
          </View>

          {/* Team profile */}
          <View
            style={{
              width: 343,
              height: 70,
              flexShrink: 0,
              borderRadius: 12,
              backgroundColor: "#fff",
              marginHorizontal: 12,
            }}
          >
            <View
              style={{
                backgroundColor: "#fff",
                justifyContent: "space-between",
                flexDirection: "row",
                width: 343,
                paddingVertical: 13,
                borderRadius: 12,
                elevation: 5,
                shadowColor: "#000",
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
              }}
            >
              <View>
                <Text
                  style={{ paddingLeft: 14, marginHorizontal: 16 }}
                  bold
                  color="#160520"
                  fontSize={14}
                >
                  Team
                </Text>
                <Text
                  style={{ paddingLeft: 14, marginHorizontal: 16 }}
                  color="#A7A7A7"
                  fontSize={14}
                >
                  React Native
                </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Image
                  source={images.PERSON1}
                  style={{
                    width: 35,
                    height: 35,
                    borderRadius: 90,
                    marginLeft: -10,
                    borderColor: "white",
                    borderWidth: 1,
                    marginTop: 5,
                  }}
                />
                <Image
                  source={images.PERSON3}
                  style={{
                    width: 35,
                    height: 35,
                    borderRadius: 90,
                    marginLeft: -10,
                    borderColor: "white",
                    borderWidth: 1,
                    marginTop: 5,
                  }}
                />
                <Image
                  source={images.PERSON4}
                  style={{
                    width: 35,
                    height: 35,
                    borderRadius: 90,
                    marginLeft: -10,
                    borderColor: "white",
                    borderWidth: 1,
                    marginTop: 5,
                  }}
                />
                <View
                  style={{
                    width: 35,
                    backgroundColor: "#C16262",
                    height: 35,
                    borderRadius: 90,
                    marginLeft: -10,
                    borderColor: "white",
                    borderWidth: 1,
                    marginTop: 5,
                    justifyContent: "center",
                    alignItems: "center",
                    marginRight: 16,
                  }}
                >
                  <Text color="white">+6</Text>
                </View>
              </View>
            </View>
          </View>

          {/* Colom option */}
          <View
            style={{
              backgroundColor: "#fff",
              width: 343,
              height: 205,
              borderRadius: 12,
              marginHorizontal: 12,
              marginVertical: 32,
              shadowColor: "#000",
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
            }}
          >
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                borderBottomColor: "#D3D3D3",
                borderBottomWidth: 2,
                marginHorizontal: 16,
                paddingVertical: 13,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image
                  source={images.GUARD}
                  style={{ width: 24, height: 24 }}
                />
                <Text style={{ paddingLeft: 14, paddingRight: 19 }}>
                  Privacy and Security
                </Text>
              </View>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width="7"
                height="13"
                viewBox="0 0 7 13"
                fill="none"
                style={{ paddingTop: 20 }}
              >
                <Path
                  d="M1 12L5.59317 7.47223C6.13561 6.93751 6.13561 6.06251 5.59317 5.52779L1 1"
                  stroke="#292D32"
                  stroke-width="2"
                  stroke-miterlimit="10"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </Svg>
            </View>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                borderBottomColor: "#D3D3D3",
                borderBottomWidth: 2,
                marginHorizontal: 16,
                paddingVertical: 13,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image source={images.HELP} style={{ width: 24, height: 24 }} />
                <Text style={{ paddingLeft: 14, paddingRight: 19 }}>Help</Text>
              </View>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width="7"
                height="13"
                viewBox="0 0 7 13"
                fill="none"
                style={{ paddingTop: 20 }}
              >
                <Path
                  d="M1 12L5.59317 7.47223C6.13561 6.93751 6.13561 6.06251 5.59317 5.52779L1 1"
                  stroke="#292D32"
                  stroke-width="2"
                  stroke-miterlimit="10"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </Svg>
            </View>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                borderBottomColor: "#D3D3D3",
                borderBottomWidth: 2,
                marginHorizontal: 16,
                paddingVertical: 13,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image
                  source={images.ABOUT}
                  style={{ width: 24, height: 24 }}
                />
                <Text style={{ paddingLeft: 14, paddingRight: 19 }}>
                  About Us
                </Text>
              </View>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width="7"
                height="13"
                viewBox="0 0 7 13"
                fill="none"
                style={{ paddingTop: 20 }}
              >
                <Path
                  d="M1 12L5.59317 7.47223C6.13561 6.93751 6.13561 6.06251 5.59317 5.52779L1 1"
                  stroke="#292D32"
                  stroke-width="2"
                  stroke-miterlimit="10"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </Svg>
            </View>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                borderBottomColor: "#D3D3D3",
                marginHorizontal: 16,
                paddingVertical: 13,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image
                  source={images.GUARD_LOGOUT}
                  style={{ width: 24, height: 24 }}
                />
                <Text style={{ paddingLeft: 14, paddingRight: 19 }}>
                  Logout
                </Text>
              </View>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width="7"
                height="13"
                viewBox="0 0 7 13"
                fill="none"
                style={{ paddingTop: 20 }}
              >
                <Path
                  d="M1 12L5.59317 7.47223C6.13561 6.93751 6.13561 6.06251 5.59317 5.52779L1 1"
                  stroke="#292D32"
                  stroke-width="2"
                  stroke-miterlimit="10"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </Svg>
            </View>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 15,
            }}
          >
            <Text bold>v0.0.1</Text>
          </View>
      </ScrollView>
    </View>
  );
};

export default Profile;