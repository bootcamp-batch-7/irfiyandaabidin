import React, { useState } from "react";
import { ImageBackground, ScrollView, TextInput, View } from "react-native";
import Text from "../../components/Text";
import Navigation from "../../components/Navigation";
import { HEIGHT, HEIGHT_WD } from "../../assets/styles";
import { NavigationContainer } from "@react-navigation/native";
import {
  ArrowRightIcon,
  BerijalanIcon,
  HomeIcon,
  MoxaIcon,
  PenIcon,
  SearchIcon,
} from "../../assets/svg";
import { TaskIcon1 } from "../../assets/svg";
import { TouchableOpacity } from "react-native-gesture-handler";
import images from "../../assets/img/index";
import { Image } from "react-native";

function Home() {
  const [viewSprint, setViewSprint] = useState(0);
  const dataSprint = [
    {
      id: 1,
      title: "Sprint 1",
      date: "Mar, 16 -17",
      progress: "100%",
      moxa: "Moxa",
      todo: "Dolore dolor adipisicing sit fugiat consectetur ex ad incididunt amet magna minim voluptate. Nisi dolore enim reprehenderit excepteur dolor non elit occaecat et sunt do ea. Sit ipsum veniam pariatur nostrud est Lorem do reprehenderit fugiat ullamco commodo. Duis Lorem culpa ex reprehenderit exercitation. Laborum elit duis cupidatat anim velit. Quis nulla anim eu velit fugiat nulla aliqua. Excepteur excepteur ea esse do deserunt minim commodo cillum id anim consequat.",
      status: "Done",
      person: [images.PERSON1, images.PERSON2]
    },
    {
      id: 2,
      title: "Sprint 2",
      date: "Mar, 19 - 21",
      progress: "70%",
      moxa: "Moxa",
      todo: "Dolor amet exercitation sint ad proident officia est in. Deserunt consectetur labore culpa in enim do ea dolore officia velit id ullamco nisi enim. Ut laboris reprehenderit est cupidatat ex enim. Est occaecat exercitation mollit commodo officia reprehenderit adipisicing proident ad cupidatat quis duis aliquip ex. Ea cupidatat tempor fugiat veniam. Qui non elit esse culpa sunt duis cupidatat ullamco laboris non minim. Minim eu id dolor quis fugiat in deserunt irure ad nostrud Lorem deserunt.",
      status: "In Progress",
      person: [images.PERSON1, images.PERSON2]
    },
    {
      id: 3,
      title: "Sprint 3",
      date: "Mar, 19 - 21",
      progress: "0%",
      moxa: "Moxa",
      todo: "Aliquip aliquip aute aute consectetur ut cillum veniam dolor velit consequat. Laborum elit esse esse nostrud do consectetur in proident et aute proident dolore. Tempor enim anim aute ullamco consequat proident commodo ullamco.",
      status: "Not Started",
      person: [images.PERSON1, images.PERSON2]
    },
    {
      id: 4,
      title: "Sprint 4",
      date: "Apr, 02 - 03",
      progress: "20%",
      moxa: "Moxa",
      todo: "Laboris esse magna duis quis ad mollit. Deserunt aute eiusmod sunt eiusmod aute adipisicing irure dolor Lorem elit aliqua. Do pariatur anim officia laboris minim consectetur ullamco exercitation esse. Ea exercitation commodo exercitation voluptate consectetur.",
      status: "On Progress",
      person: [images.PERSON1, images.PERSON2]
    },
    {
      id: 5,
      title: "Sprint 5",
      date: "Apr, 07 - 09",
      progress: "100%",
      moxa: "Moxa",
      todo: "Irure aliqua nisi eu quis id commodo ullamco reprehenderit veniam mollit. Excepteur aute nostrud quis proident dolore ipsum anim. Deserunt deserunt deserunt occaecat ea. Laboris sit fugiat enim sunt minim ex laboris exercitation dolor voluptate qui consequat commodo.",
      status: "Done",
      person: [images.PERSON1, images.PERSON2]
    },
  ];

  return (
    <ScrollView>
      <View style={{ height: HEIGHT_WD, backgroundColor: "#fff" }}>
        <View style={{ flexDirection: "row", paddingTop: 30, marginLeft: 16 }}>
          <TaskIcon1 width={52} height={52} />
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingLeft: 20,
              marginLeft: 16,
              borderRadius: 80,
              backgroundColor: "#FBFBFB",
              width: 275,
              height: 52,
            }}
          >
            <SearchIcon />
            <TextInput
              style={{ paddingLeft: 5 }}
              placeholder="Find Your Projects"
            />
          </View>
        </View>
        <View
          style={{
            justifyContent: "space-between",
            marginVertical: 20,
            marginLeft: 16,
            flexDirection: "row",
          }}
        >
          <View>
            <Text bold fontSize={30} color="#261A31">
              Projects
            </Text>
            <Text color="#A7A7A7">You have 99 projects</Text>
          </View>
          <TouchableOpacity
            style={{
              justifyContent: "center",
              alignItems: "center",
              width: 94,
              height: 37,
              backgroundColor: "#F5F3F6",
              borderRadius: 80,
              marginRight: 16,
            }}
          >
            <Text bold>+ Add</Text>
          </TouchableOpacity>
        </View>
        <View>
          <ScrollView horizontal={true} style={{}}>
            <View
              style={{
                backgroundColor: "#032A50",
                width: 215,
                height: 295,
                marginHorizontal: 8,
                borderRadius: 12,
                overflow: "hidden",
              }}
            >
              <View style={{ borderRadius: 12 }}>
                <View
                  style={{
                    marginLeft: 37,
                    marginTop: 55,
                    marginRight: -54,
                    marginBottom: -6,
                    position: "absolute",
                    transform: [{ rotate: "15deg" }],
                  }}
                >
                  <MoxaIcon />
                </View>
                <Text
                  bold
                  fontSize={24}
                  color="#fff"
                  style={{ marginTop: 20, marginLeft: 20 }}
                >
                  Berijalan
                </Text>
                <Text color="#fff" style={{ marginLeft: 20 }} semiBold>
                  Setir Kanan Project
                </Text>
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20,
                    marginTop: 10,
                    marginBottom: 65,
                  }}
                >
                  <Image
                    source={images.PERSON1}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                  <Image
                    source={images.PERSON2}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      marginTop: -13,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                  <Image
                    source={images.PERSON3}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      marginTop: -13,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                </View>
                <TouchableOpacity
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    backgroundColor: "#C16262",
                    width: 205,
                    height: 62,
                    marginHorizontal: 5,
                    borderRadius: 12,
                    padding: 20,
                  }}
                >
                  <Text fontSize={16} color={"#FFF"} bold>
                    View Project
                  </Text>
                  <ArrowRightIcon />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                backgroundColor: "#3CB8D3",
                width: 215,
                height: 295,
                marginHorizontal: 8,
                borderRadius: 12,
                overflow: "hidden",
              }}
            >
              <View style={{ borderRadius: 12 }}>
                <View
                  style={{
                    marginLeft: 64,
                    marginTop: 65,
                    marginRight: -58,
                    marginBottom: -16,
                    position: "absolute",
                    transform: [{ rotate: "15deg" }],
                  }}
                >
                  <BerijalanIcon />
                </View>
                <Text
                  bold
                  fontSize={24}
                  color="#fff"
                  style={{ marginTop: 20, marginLeft: 20 }}
                >
                  Berijalan
                </Text>
                <Text color="#fff" style={{ marginLeft: 20 }} semiBold>
                  Berijalan Project
                </Text>
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20,
                    marginTop: 10,
                    marginBottom: 65,
                  }}
                >
                  <Image
                    source={images.PERSON1}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                  <Image
                    source={images.PERSON2}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      marginTop: -13,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                  <Image
                    source={images.PERSON3}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      marginTop: -13,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                </View>
                <TouchableOpacity
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    backgroundColor: "#C16262",
                    width: 205,
                    height: 62,
                    marginHorizontal: 5,
                    borderRadius: 12,
                    padding: 20,
                  }}
                >
                  <Text fontSize={16} color={"#FFF"} bold>
                    View Project
                  </Text>
                  <ArrowRightIcon />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                backgroundColor: "#032A50",
                width: 215,
                height: 295,
                marginHorizontal: 8,
                borderRadius: 12,
                overflow: "hidden",
              }}
            >
              <View style={{ borderRadius: 12 }}>
                <View
                  style={{
                    marginLeft: 37,
                    marginTop: 55,
                    marginRight: -54,
                    marginBottom: -6,
                    position: "absolute",
                    transform: [{ rotate: "15deg" }],
                  }}
                >
                  <MoxaIcon />
                </View>
                <Text
                  bold
                  fontSize={24}
                  color="#fff"
                  style={{ marginTop: 20, marginLeft: 20 }}
                >
                  Berijalan
                </Text>
                <Text color="#fff" style={{ marginLeft: 20 }} semiBold>
                  Setir Kanan Project
                </Text>
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20,
                    marginTop: 10,
                    marginBottom: 65,
                  }}
                >
                  <Image
                    source={images.PERSON1}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                  <Image
                    source={images.PERSON2}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      marginTop: -13,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                  <Image
                    source={images.PERSON3}
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 90,
                      marginTop: -13,
                      borderColor: "#fff",
                      borderWidth: 1,
                    }}
                  />
                </View>
                <TouchableOpacity
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    backgroundColor: "#C16262",
                    width: 205,
                    height: 62,
                    marginHorizontal: 5,
                    borderRadius: 12,
                    padding: 20,
                  }}
                >
                  <Text fontSize={16} color={"#FFF"} bold>
                    View Project
                  </Text>
                  <ArrowRightIcon />
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
        <View style={{ marginTop: 26, marginHorizontal: 16 }}>
          <Text fontSize={30} bold>
            Time Management
          </Text>
          <View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              {dataSprint.map((value) => (
                <View style={{ marginRight: 40 }}>
                  <TouchableOpacity onPress={() => setViewSprint(value.id)}>
                    <Text
                      fontSize={14}
                      color={viewSprint == value.id ? "#000" : "#909090"}
                    >
                      {value.title}
                    </Text>
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
            {viewSprint ? (
              <View style={{ flexDirection: "row", marginTop: 29 }}>
                <View style={{ marginRight: 85 }}>
                  <Text bold fontSize={16}>
                    {dataSprint.find((value) => value.id == viewSprint).date}
                  </Text>
                  <Text fontSize={16} color="#909090">
                    Progress{" "}
                    {
                      dataSprint.find((value) => value.id == viewSprint)
                        .progress
                    }
                  </Text>
                </View>
                <View
                  style={{
                    borderColor: "#909090",
                    borderWidth: 1.5,
                    borderRadius: 90,
                  }}
                >
                  <Text style={{ paddingHorizontal: 33, paddingVertical: 11 }}>
                    {dataSprint.find((value) => value.id == viewSprint).status}
                  </Text>
                </View>
              </View>
            ) : (
              <View></View>
            )}
          </View>
        </View>
      </View>
      <View style={{ backgroundColor: "#F9F3FF" ,flexDirection: "row", paddingTop: 42 }}>
        <View style={{ }}>
          {dataSprint.map((value) => (
            <View style={{ flexDirection: "row" }}>
              <View style={{ marginLeft: 16, marginRight: 19,alignItems: "center" }}>
                <Text>{value.date}</Text>
                <View
                  style={{
                    alignContent: "center",
                    borderStyle: "dashed",
                    width: 1,
                    borderColor: "#A7A7A7",
                    borderWidth: 1,
                    height: 186,
                  }}
                ></View>
              </View>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {dataSprint.map((value) => (
                  <View style={{ flexDirection: "row" }}>
                    <View
                      style={{
                        padding: 16,
                        width: 182,
                        marginRight: 16,
                        backgroundColor: "#fff",
                        marginBottom: 39,
                        borderRadius: 12,
                        elevation: 5,
                        shadowColor: "#000",
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                      }}
                    >
                      <View>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={{ marginRight: 91 }} fontSize={18} bold >{value.moxa}</Text>
                          <TouchableOpacity>
                            <PenIcon />
                          </TouchableOpacity>
                        </View>
                        <View style={{flexDirection: "row", marginLeft: 10}}>
                          {value.person.map((value) => (
                            <Image source={value} style={{ borderColor: "#fff", borderWidth: 1, borderRadius:90, width: 24, height: 24, marginLeft: -10}}></Image>
                          ))}
                        </View>
                        <View style={{ marginTop: 45 }}>
                          <Text bold fontSize={18}>
                            Todo
                          </Text>
                          <Text numberOfLines={2}>{value.todo}</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                ))}
              </ScrollView>
            </View>
          ))}
          <View></View>
        </View>
        <View></View>
      </View>
    </ScrollView>
  );
}

export default Home;
