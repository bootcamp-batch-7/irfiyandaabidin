import React from "react";
import { View } from "react-native";
import Text from "../../components/Text";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "../Home";
import Profile from "../Profile";
import { WIDTH } from "../../assets/styles";
import { TouchableOpacity } from "react-native";
import { HomeIcon, PerformIcon, ProfileIcon, TaskIcon } from "../../assets/svg";
import Svg, { Path, Circle } from "react-native-svg";
import { LinearGradient } from "expo-linear-gradient";
import Task from "../Task";
import Performance from "../Performance";
import AddTask from "../AddTask";

const Tab = createBottomTabNavigator();

function Main({ navigation, router }) {
  return (
    <View style={{ flex: 1 }}>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarActiveTintColor: "#04325F",
          tabBarInactiveTintColor: "#CED1D4",
          tabBarIcon: ({ focused, color, size }) => {
            let tabsIcon = {
              Home: (
                <HomeIcon
                  width={focused ? 40 : 25}
                  height={focused ? 40 : 25}
                  fill={color}
                />
              ),
              Profile: (
                <ProfileIcon
                  width={24}
                  height={focused ? 40 : 25}
                  fill={color}
                />
              ),
              Task: (
                <TaskIcon width={24} height={focused ? 40 : 25} fill={color} />
              ),
              Performance: (
                <PerformIcon
                  width={focused ? 40 : 25}
                  height={focused ? 40 : 25}
                  fill={color}
                />
              ),
            };
            return <View>{tabsIcon[route.name]}</View>;
          },
          tabBarLabel: ({ focused, color }) => (
            <Text
              bold={focused}
              fontSize={10}
              color={focused ? "#04325F" : "#CED1D4"}
            >
              {route.name}
            </Text>
          ),
          tabBarStyle: {
            paddingVertical: 10,
            height: 70,
          },
        })}
      >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Profile" component={Profile} />
        <Tab.Screen
          name="AddTask"
          component={AddTask}
          options={{
            tabBarButton: ({onPress}) => (
              <TouchableOpacity
                onPress={onPress}
                style={{
                  top: -20,
                }}
              >
                <LinearGradient
                  colors={["#00629C", "#003362"]}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  style={{
                    flex: 1,
                    borderRadius: 90,
                    padding: 12,
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 5,
                  }}
                >
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="36"
                    height="36"
                    viewBox="0 0 36 36"
                    fill="none"
                  >
                    <Path
                      fill-rule="evenodd"
                      clip-rule="evenodd"
                      d="M19.7738 5.29176C19.6706 4.40365 18.9158 3.71429 18.0001 3.71429C17.0138 3.71429 16.2144 4.51379 16.2144 5.50001V16.2143H5.50007L5.29182 16.2263C4.40371 16.3295 3.71436 17.0842 3.71436 18C3.71436 18.9862 4.51385 19.7857 5.50007 19.7857H16.2144V30.5L16.2264 30.7083C16.3295 31.5964 17.0843 32.2857 18.0001 32.2857C18.9863 32.2857 19.7858 31.4862 19.7858 30.5V19.7857H30.5001L30.7083 19.7737C31.5964 19.6706 32.2858 18.9158 32.2858 18C32.2858 17.0138 31.4863 16.2143 30.5001 16.2143H19.7858V5.50001L19.7738 5.29176Z"
                      fill="white"
                    />
                  </Svg>
                </LinearGradient>
              </TouchableOpacity>
            ),
          }}
        />
        <Tab.Screen name="Task" component={Task} />
        <Tab.Screen name="Performance" component={Performance} />
      </Tab.Navigator>
    </View>
  );
}

export default Main;
