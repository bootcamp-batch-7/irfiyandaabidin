import React, { useState } from "react";
import Text from "../../components/Text";
import { LineChart } from "react-native-gifted-charts";
import { View } from "react-native";
import { WIDTH } from "../../assets/styles";
import { Link } from "@react-navigation/native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { ArrowBottomIcon, SetirKananIcon } from "../../assets/svg";
import images from "../../assets/img/index";
import { Image } from "react-native";
import * as Progress from "react-native-progress";

function Performance() {
  const dataChart = [{ value: 3 }, { value: 5 }, { value: 4 }, { value: 7 }];
  const [detailProject, setDetailProject] = useState({});
  const handleDetailProject = (dataId) => {
    setDetailProject((prevState) => ({
      ...prevState,
      [dataId]: !prevState[dataId],
    }));
  };
  const dataProject = [
    {
      id: 1,
      titleProject: "Registration - New Features",
      descriptionProject: "NextJS WEB",
      status: "On Progress",
      progress: 0.6,
      day: "2 day left",
    },
    {
      id: 2,
      titleProject: "Project 2",
      descriptionProject: "Description 2",
      status: "On Progress",
      progress: 0.8,
      day: "1 day left",
    },
    {
      id: 3,
      titleProject: "Project 3",
      descriptionProject: "Description 3",
      status: "On Progress",
      progress: 0.4,
      day: "3 day left",
    },
  ];

  return (
    <ScrollView>
      <View style={{ paddingHorizontal: 17, backgroundColor: "#fff"}}>
        <Text fontSize={30} bold style={{ marginTop: 42 }}>
          Productivity
        </Text>
        <View
          style={{
            marginTop: 10,
            width: 343,
            height: 240,
            backgroundColor: "#F8F3FF",
            borderRadius: 16,
          }}
        >
          <LineChart
            areaChart
            data={dataChart}
            startFillColor="#76AEE3"
            startOpacity={0.8}
            endFillColor="#D9D9D9"
            endOpacity={0.3}
            width={WIDTH}
            color="#04325F"
            xAxisColor="#F8F3FF"
            yAxisColor="#F8F3FF"
            isAnimated
            animationDuration={1000}
            hideDataPoints
          />
        </View>
        <View style={{ marginTop: 10, flexDirection: "row" }}>
          <Text fontSize={30} bold>
            Recent Projects
          </Text>
          <Text style={{ position: "absolute", right: 0, bottom: 10 }}>
            See All
          </Text>
        </View>
        {dataProject.map((item) => (
          <TouchableOpacity
            onPress={() => handleDetailProject(item.id)}
            key={item.id}
            style={{
              backgroundColor: "#F8F3FF",
              width: "100%",
              height: detailProject[item.id] ? "162" : "64",
              borderRadius: 12,
              paddingHorizontal: 16,
              marginVertical: 5,
              paddingVertical: 11
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <SetirKananIcon style={{ marginRight: 12 }} />
              <View>
                <Text fontSize={16} bold>
                  {item.titleProject}
                </Text>
                <Text color="#909090" fontSize={14}>
                  {item.descriptionProject}
                </Text>
              </View>
              <ArrowBottomIcon style={{ right: 0 }} />
            </View>
            {detailProject[item.id] && (
              <View style={{ paddingTop: 22 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text color={"#C89C02"}>{item.status} </Text>
                  <Text color={"#909090"}>{item.day}</Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    
                  }}
                >
                  <View>
                    <View style={{ flexDirection: "row", paddingTop: 10 }}>
                      <Image
                        source={images.PERSON1}
                        style={{
                          width: 35,
                          height: 35,
                          borderRadius: 90,
                          marginLeft: -10,
                          borderColor: "white",
                          borderWidth: 1,
                          marginTop: 5,
                        }}
                      />
                      <Image
                        source={images.PERSON3}
                        style={{
                          width: 35,
                          height: 35,
                          borderRadius: 90,
                          marginLeft: -10,
                          borderColor: "white",
                          borderWidth: 1,
                          marginTop: 5,
                        }}
                      />
                      <Image
                        source={images.PERSON4}
                        style={{
                          width: 35,
                          height: 35,
                          borderRadius: 90,
                          marginLeft: -10,
                          borderColor: "white",
                          borderWidth: 1,
                          marginTop: 5,
                        }}
                      />
                      <Image
                        source={images.PERSON4}
                        style={{
                          width: 35,
                          height: 35,
                          borderRadius: 90,
                          marginLeft: -10,
                          borderColor: "white",
                          borderWidth: 1,
                          marginTop: 5,
                        }}
                      />
                    </View>
                  </View>
                  <View style={{justifyContent: "center", alignItem: "center", }}>
                    <View style={{ flexDirection: "row" }}>
                      <Text fontSize={18} bold>
                        {item.progress * 100}%
                      </Text>
                      <View
                        style={{ justifyContent: "center", paddingLeft: 5 }}
                      >
                        <Progress.Bar
                          progress={item.progress}
                          color="#F8A54B"
                          width={107}
                          height={10}
                        />
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{ alignSelf: "flex-end", }}>
                  <Text fontSize={12}>5 of 12 Task Completed</Text>
                </View>

              </View>
            )}
          </TouchableOpacity>
        ))}
      </View>
    </ScrollView>
  );
}

export default Performance;
