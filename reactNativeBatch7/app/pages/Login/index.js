import React, { useEffect, useState } from "react";
import { TextInput, View, ActivityIndicator } from "react-native";
import Text from "../../components/Text";
import Images from "../../assets/img/index";
import { ImageBackground } from "react-native";
import { HEIGHT, Fonts } from "../../assets/styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { EyeIcon, EyeSlashIcon } from "../../assets/svg";
import Satellite from "../../services/satellite";

function Login({ navigation, router }) {
  const [isEnable, setIsEnable] = useState(false);
  const [showPassword, setShowPassword] = useState(true);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [errorEmail, setErrorEmail] = useState("");
  const [errorPassword, setErrorPassword] = useState("");

  const handlerShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const [isLoading, setIsLoading] =  useState(false);

  useEffect(() => {
    if( email !== "" &&
        password !== "" && 
        errorEmail === "" && 
        errorPassword === "") {
        setIsEnable(true)
    } else {
        setIsEnable(false)
    }
  }, [email, password, errorEmail, errorPassword])

  const onSubmit = async () => {
    setIsLoading(true)
    const body = {
        email: email,
        password: password
    }; 
    // try {
    //     const response = await Satellite.post("auth/login", body)
    // } catch (err) {
    //     console.log("ERROR ", err)
    // }

    await Satellite.post("auth/login", body)
    .then((res) => {
        if(res == undefined ){
            return setErrorPassword("Invalid Email or Password")
        }
        // console.log(res)
        res.status == 200 ? navigation.navigate("Main") : error;
    })
    .catch((error) => {
        console.log(error)
    })
    .finally(() => {
        setTimeout(() => {
            setIsLoading(false)
        }, 100)
    })
  }

  return (
    <View>
      <ImageBackground
        style={{ height: HEIGHT, paddingHorizontal: 31 }}
        source={Images.BG_SCREEN}
      >
        <View style={{ paddingTop: 122, paddingBottom: 27 }}>
          <Text fontSize={16} bold color="#D3D3D3" style={{ paddingBottom: 8 }}>
            Email
          </Text>
          <View>
          <TextInput
            onChangeText={(value) => {
                setEmail(value)
                const regexEmail =  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
                if(value === "") {
                    setErrorEmail("email must be filled in")
                    return
                }
                if(!regexEmail.test(value)) {
                    setErrorEmail("Invalid Mail Address")
                    return 
                } else {
                    setErrorEmail("");
                }
            }}
            autoCapitalize="none"
            style={{
              fontFamily: Fonts.Nunito.Bold,
              backgroundColor: "#273C75",
              height: 46,
              borderRadius: 8,
              padding: 12,
              borderColor: "#132040",
              color: "#D3D3D3",
            }}
            placeholder="Enter Your Email"
            placeholderTextColor={"#D3D3D3"}
          ></TextInput>
          </View>
        <Text color="#EA8685" fontSize={10} style={{ textAlign: "right",marginRight: 8}}>{errorEmail}</Text>
        </View>
        <View style={{ paddingBottom: 27 }}>
          <Text fontSize={16} bold color="#D3D3D3" style={{ paddingBottom: 8 }}>
            Password
          </Text>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              backgroundColor: "#273C75",
              borderRadius: 8,
              padding: 12,
              borderColor: "#132040",
              justifyContent: "space-between"
            }}
          >
            <TextInput
              color
              secureTextEntry={showPassword}
              style={{ fontFamily: Fonts.Nunito.Bold, color: "#D3D3D3" }}
              placeholder="Password"
              placeholderTextColor={"#D3D3D3"}
              disabled={true}
              onChangeText={(value) => {
                setPassword(value)
                if (value === "") {
                    setErrorPassword("Password must be filled in")
                    return 
                }
                setErrorPassword("");
              }}
            ></TextInput>
            <TouchableOpacity onPress={() => handlerShowPassword()}>
              {showPassword ? (
                <EyeIcon width={20} height={20} />
              ) : (
                <EyeSlashIcon width={20} height={20} />
              )}
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: "row"}}>
            <Text color={"#F6E58D"} fontSize={10} style={{ paddingTop: 5 }}>
                Forgot Password ?
            </Text>
            <Text color="#EA8685" fontSize={10} style={{ flex:1,textAlign: "right",marginRight: 8}}>{errorPassword}</Text>
          </View>
        </View>
        <TouchableOpacity
          disabled={!isEnable || isLoading}
          onPress={onSubmit}
          style={{
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: isEnable ? "#18DCFF" : "#2281B0",
            borderRadius: 8,
            height: 64,
          }}
        >
            {isLoading ? (
              <ActivityIndicator size="large" color="#fff" />
            ) :
             (<Text bold>Login</Text>)
            }
        </TouchableOpacity>
        <View style={{ alignItems: "center" }}>
          <Text color="#D3D3D3" bold style={{ marginTop: 20 }}>
            Dont Have an Account?
            <TouchableOpacity onPress={() => navigation.navigate("Register")}>
              <Text color="#F6E58D" bold>
                Sign Up
              </Text>
            </TouchableOpacity>
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
}

export default Login;
