export default {
    BG_PROFILE: require('./bgProfile.png'),
    PERSON1: require('./person1.png'),
    PERSON2: require('./person2.png'),
    PERSON3: require('./person3.png'),
    PERSON4: require('./person4.png'),
    FAVICON: require('./favicon.png'),
    ADAPTIVE_ICON: require('./adaptive-icon.png'),
    GUARD: require('./guard.png'),
    GUARD_LOGOUT: require('./guard_logout.png'),
    HELP: require('./help.png'),
    ICON: require('./icon.png'),
    SPLASH: require('./splash.png'),
    ABOUT: require('./about.png'),
    MOXA: require('./moxa.jpg'),
    BG_SCREEN: require('./bgscreen1.png')
}