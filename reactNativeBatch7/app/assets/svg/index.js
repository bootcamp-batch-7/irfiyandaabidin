import HomeIcon from "./home.svg";
import TaskIcon from "./task.svg";
import PerformIcon from "./perform.svg";
import ProfileIcon from "./profile.svg";
import VectorIcon from "./Vector.svg";
import TaskIcon1 from "./Task1.svg";
import SearchIcon from "./Search.svg";
import ArrowRightIcon from "./ArrowRight.svg";
import MoxaIcon from "./Moxa.svg";
import BerijalanIcon from "./Berijalan.svg"
import SetirKananIcon from "./setirkanan.svg";
import ArrowBottomIcon from "./ArrowBottom.svg";
import EyeIcon from "./eye.svg";
import EyeSlashIcon from "./eyeSlash.svg";
import FlagIcon from "./flag.svg";
import ArrowLeftIcon from "./ArrowLeft.svg"
import PenIcon from "./Pen.svg";

export { HomeIcon, TaskIcon,
    PerformIcon,
    ProfileIcon,
    VectorIcon,
    TaskIcon1,
    SearchIcon,
    ArrowRightIcon,
    MoxaIcon,
    BerijalanIcon,
    SetirKananIcon,
    ArrowBottomIcon,
    EyeIcon,
    EyeSlashIcon,
    FlagIcon,
    ArrowLeftIcon,
    PenIcon
};