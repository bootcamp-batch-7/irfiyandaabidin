import { View } from 'react-native'
import React from 'react'
import Home from './pages/Home'
import { NavigationContainer } from '@react-navigation/native'
import Routes from './services/router'

function IndexLayout() {
  return (
    <NavigationContainer>
      <Routes/>
    </NavigationContainer>
  )
}

module.exports = {
  IndexLayout
}