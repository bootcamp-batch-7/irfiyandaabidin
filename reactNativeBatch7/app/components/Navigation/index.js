import { ImageBackground, View, Image, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import Text from "../../components/Text/index";
import images from "../../assets/img/index";
import { WIDTH } from "../../assets/styles";
import { ScrollView } from "react-native";
import Svg, { Path, Circle } from "react-native-svg";
import { LinearGradient } from "expo-linear-gradient";
import { HomeIcon } from "../../assets/svg";

function Navigation() {
    const [isPressed, setIsPressed] = useState(false);
  
    const handlePress = () => {
      setIsPressed(!isPressed);
    }
  
    const iconColor = isPressed ? "#04325F" : "#CED1D4"
  
  return (
    <View
    style={{
      backgroundColor: "#fff",
      width: WIDTH,
      height: 65,
      paddingTop: 12,
      justifyContent: "space-evenly",
      flexDirection: "row",
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 15,
    }}
  >
    <TouchableOpacity style={{ alignItems: "center" }} onPress={handlePress}>
      <HomeIcon width={24} height={25} fill={iconColor}/>
      <Text fontSize={10}>Home</Text>
    </TouchableOpacity>
    <View style={{ alignItems: "center",paddingLeft:15 }}>
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        width="21"
        height="26"
        viewBox="0 0 21 26"
        fill="none"
      >
        <Path
          d="M15.4511 2.89707C15.4511 4.49368 14.1506 5.79415 12.554 5.79415H8.04746C7.24916 5.79415 6.52811 5.47225 6.0002 4.94434C5.47229 4.41643 5.15039 3.69538 5.15039 2.89707C5.15039 1.30046 6.45085 0 8.04746 0H12.554C13.3523 0 14.0734 0.321897 14.6013 0.849808C15.1292 1.37772 15.4511 2.09877 15.4511 2.89707Z"
          fill="#CED1D4"
        />
        <Path
          d="M19.0949 3.89978C18.7988 3.65514 18.464 3.462 18.1035 3.32036C17.7301 3.17873 17.3567 3.47488 17.2794 3.86115C16.8416 6.06293 14.8974 7.72392 12.554 7.72392H8.04742C6.75984 7.72392 5.5495 7.22176 4.63532 6.30757C3.96577 5.63802 3.50224 4.78821 3.32198 3.87403C3.24472 3.48775 2.85844 3.17873 2.48504 3.33324C0.991442 3.93841 0 5.30325 0 8.04581V20.5997C0 24.4625 2.30478 25.7501 5.15035 25.7501H15.4511C18.2966 25.7501 20.6014 24.4625 20.6014 20.5997V8.04581C20.6014 5.94704 20.022 4.65946 19.0949 3.89978ZM5.15035 13.1961H10.3007C10.8286 13.1961 11.2664 13.6339 11.2664 14.1618C11.2664 14.6897 10.8286 15.1275 10.3007 15.1275H5.15035C4.62244 15.1275 4.18466 14.6897 4.18466 14.1618C4.18466 13.6339 4.62244 13.1961 5.15035 13.1961ZM15.4511 20.2778H5.15035C4.62244 20.2778 4.18466 19.8401 4.18466 19.3122C4.18466 18.7842 4.62244 18.3465 5.15035 18.3465H15.4511C15.979 18.3465 16.4167 18.7842 16.4167 19.3122C16.4167 19.8401 15.979 20.2778 15.4511 20.2778Z"
          fill="#CED1D4"
        />
      </Svg>
      <Text fontSize={10}>Task</Text>
    </View>
    <View
    style={{
      alignItems: "center",
      width: 60,
      height: 60,
      paddingStart: 20,
      marginTop: -30,
    }}
  >
    <LinearGradient
      colors={["#00629C", "#003362"]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      style={{
        flex: 1,
        borderRadius: 90,
        padding: 12,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      }}
    >
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        width="36"
        height="36"
        viewBox="0 0 36 36"
        fill="none"
      >
        <Path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M19.7738 5.29176C19.6706 4.40365 18.9158 3.71429 18.0001 3.71429C17.0138 3.71429 16.2144 4.51379 16.2144 5.50001V16.2143H5.50007L5.29182 16.2263C4.40371 16.3295 3.71436 17.0842 3.71436 18C3.71436 18.9862 4.51385 19.7857 5.50007 19.7857H16.2144V30.5L16.2264 30.7083C16.3295 31.5964 17.0843 32.2857 18.0001 32.2857C18.9863 32.2857 19.7858 31.4862 19.7858 30.5V19.7857H30.5001L30.7083 19.7737C31.5964 19.6706 32.2858 18.9158 32.2858 18C32.2858 17.0138 31.4863 16.2143 30.5001 16.2143H19.7858V5.50001L19.7738 5.29176Z"
          fill="white"
        />
      </Svg>
    </LinearGradient>
  </View>
    <View style={{ alignItems: "center" }}>
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        width="26"
        height="26"
        viewBox="0 0 26 26"
        fill="none"
      >
        <Path
          d="M5.86371 15.9551H2.51122C1.13005 15.9551 0 17.0852 0 18.4663V24.7444C0 25.435 0.565025 26 1.25561 26H5.86371C6.5543 26 7.11932 25.435 7.11932 24.7444V17.2107C7.11932 16.5201 6.5543 15.9551 5.86371 15.9551Z"
          fill="#CED1D4"
        />
        <Path
          d="M14.2263 10.9326H10.8738C9.4926 10.9326 8.36255 12.0627 8.36255 13.4439V24.7444C8.36255 25.435 8.92757 26 9.61816 26H15.4819C16.1725 26 16.7375 25.435 16.7375 24.7444V13.4439C16.7375 12.0627 15.62 10.9326 14.2263 10.9326Z"
          fill="#CED1D4"
        />
        <Path
          d="M22.6011 19.7219H19.2487C18.5581 19.7219 17.993 20.2869 17.993 20.9775V24.7444C17.993 25.435 18.5581 26 19.2487 26H23.8568C24.5473 26 25.1124 25.435 25.1124 24.7444V22.2331C25.1124 20.852 23.9823 19.7219 22.6011 19.7219Z"
          fill="#CED1D4"
        />
        <Path
          d="M16.3349 4.46684C16.7242 4.0776 16.8749 3.61302 16.7493 3.21123C16.6237 2.80943 16.2345 2.52064 15.682 2.43275L14.4766 2.23185C14.4264 2.23185 14.3134 2.14396 14.2883 2.09373L13.6228 0.762784C13.1206 -0.254261 11.978 -0.254261 11.4757 0.762784L10.8102 2.09373C10.7977 2.14396 10.6847 2.23185 10.6345 2.23185L9.42901 2.43275C8.87654 2.52064 8.49986 2.80943 8.36174 3.21123C8.23618 3.61302 8.38685 4.0776 8.77609 4.46684L9.70524 5.40855C9.75547 5.44622 9.79314 5.59689 9.78058 5.64711L9.5169 6.80228C9.316 7.66865 9.64246 8.05789 9.85592 8.20856C10.0694 8.35924 10.534 8.56013 11.2999 8.10811L12.43 7.44264C12.4802 7.40497 12.6434 7.40497 12.6937 7.44264L13.8112 8.10811C14.1627 8.32157 14.4515 8.38435 14.6775 8.38435C14.9412 8.38435 15.1296 8.2839 15.2426 8.20856C15.456 8.05789 15.7825 7.66865 15.5816 6.80228L15.3179 5.64711C15.3053 5.58433 15.343 5.44622 15.3932 5.40855L16.3349 4.46684Z"
          fill="#CED1D4"
        />
      </Svg>
      <Text fontSize={10}>Performance</Text>
    </View>
    <View style={{ alignItems: "center" }}>
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        width="21"
        height="25"
        viewBox="0 0 21 25"
        fill="none"
      >
        <Path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M5.92007 13.6692C2.49272 14.7171 0 17.9054 0 21.6758C0 23.1958 1.23218 24.4279 2.75215 24.4279H18.2478C19.7678 24.4279 21 23.1958 21 21.6758C21 17.8805 18.4743 14.6751 15.0119 13.6487C13.7709 14.5723 12.183 15.1266 10.4522 15.1266C8.73422 15.1266 7.15695 14.5805 5.92007 13.6692Z"
          fill="#04325F"
        />
        <Circle cx="10.452" cy="6.71233" r="6.71233" fill="#04325F" />
      </Svg>
      <Text fontSize={10}>Profile</Text>
    </View>
  </View>
  )
}

export default Navigation