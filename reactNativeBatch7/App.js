import { SafeAreaProvider } from "react-native-safe-area-context"
import React, { useEffect } from 'react'
import {IndexLayout} from "./app/index"
import * as SplashScreen from 'expo-splash-screen';
import fonts from "./app/assets/font";
import { Text } from "react-native";
import * as Font from "expo-font";

export default function App() {
  async function loadFonts() {
    try{
      await Font.loadAsync(fonts);
    } catch (error) {
      console.warn(error);
    } finally {
      await SplashScreen.hideAsync();
    }
  }
  useEffect(() => {
    loadFonts();
  }, [])

  return (
  <SafeAreaProvider style={{flex:1}}>
    <IndexLayout />
  </SafeAreaProvider>
  );
}